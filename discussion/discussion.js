// MongoDB Aggregation

db.course_bookings.insertMany([
    {
        "courseId" : "C001", 
        "studentId": "S004", 
        "isCompleted": true
    },
    {
        "courseId" : "C002", 
        "studentId": "S001", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S002", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S004", 
        "isCompleted": false
    },
    {
        "courseId" : "C002", 
        "studentId": "S007", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S005", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S008", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S0013", 
        "isCompleted": false
    }
]);

/*
    Syntax:
        db.collectionName.aggregate([
            {
                {
                    $group: {
                        _id: <expresseion>, <"field">: {<accumulator> : <expression1>}
                    }
                }
            }

        ])

*/

// Aggregation allows us to retrieve a group of data based on specific conditions. In this case, we are retrieving or grouping the data inside our course_booking table


db.course_bookings.aggregate([
        {
            $group: {_id: null, count: {$sum: 1}}
        }
    ]);

// $match is a condition that has to be met in order for MongoDB to return the data. In this case, we are trying to get all the fields where "isCompleted" is equal to "true".

db.course_bookings.aggregate([
        {
            $match: {"isCompleted": true}
        },
        {
            $group: {_id: "$courseId", total: {$sum: 1}}
        }
    ]);

// $project basically either shows or hide a field (to include, use 1; to excluse, use 0)
db.course_bookings.aggregate([
        {
            $match: {"isCompleted": true}
        },
        {
            $project: {"studentId": 1}
        }
    ]);


// $sort organizes the returned data/documents in ascending(1) or descending order (-1).

db.course_bookings.aggregate([
        {
            $match: {"isCompleted": true}
        },
        {
            $sort: {"courseId": -1}
        }
    ]);

/*
Mini Activity:
        Aggregate the documents in course_booking collections using 2 pipeline stages:
            Item 1: 
                a. In stage 1, use the $match operator to get all documents with true value in isCompleted field.
                b. in stage 2, use the $group operator, assign an _id to the new set of documents and count the total of those who completed the course.
            
            Item 2:
                a. In stage 1, use the $match operator to get all documents with true value in isCompleted field.
                b. in stage 2, use the $sort operation to arrange the courseId in descending order while the studentId in ascending order.
*/

// Solution:

db.course_bookings.aggregate([
        {
            $match: {"isCompleted": true}
        },
        {
            $group: {_id: "studentId", count: {$sum:1}}
        }
    ]);

db.course_bookings.aggregate([
        {
            $match: {"isCompleted": true}
        },
        {
            $sort: {"courseId": -1, "studentId": 1}
        }
    ]);


db.orders.insertMany([
    {
        "customer_Id": "A123",
        "amount": 500,
        "status": "A"
    },
    {
        "customer_Id": "A123",
        "amount": 250,
        "status": "A"
    },
    {
        "customer_Id": "B212",
        "amount": 200,
        "status": "A"
    },
    {
        "customer_Id": "B212",
        "amount": 200,
        "status": "D"
    },
]);

// Operators are also a way to do automatic calculation/s within our query.
/*
    1. $sum - Returns a sum of numerical value and ignores non-numeric values.
    2. $max: Returns the highest expression/value for each group of documents.
    3. $min: Returns the lowest expression/value for each group of documents.
    4. $avg: Returns an average of the numerical values. Also, ignores non-numeric values.
*/

// $max operator

db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: "$customer_Id", maxAmount: {$max: "$amount"}}
        }
    ]);

db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: "$customer_Id", minAmount: {min: "$amount"}}
        }
    ]);


db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: "$customer_Id", minAmount: {$min: "$amount"}}
        }
    ]);

db.orders.aggregate([
        {
            $match: {"status": "D"}
        },
        {
            $group: {_id: "$status", maxAmount: {$max: "$amount"}}
        }
    ]);

db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: "$status", avgAmount: {$avg: "$amount"}}
        }
    ]);





db.course_bookings.aggregate([
        {
            $match: {"status": "D"}
        },
        {
            $sort: {"courseId": -1}
        }
    ]);


db.fruits.insertMany([
        {
            "name": "Banana",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 30,
            "price": 20,
            "onSale": true
        },
        {
            "name": "Mango",
            "supplier": "Mango Magic Inc.",
            "stocks": 50,
            "price": 70,
            "onSale": true
        },
        {
            "name": "Dragon Fruit",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 10,
            "price": 60,
            "onSale": true
        },
        {
            "name": "Grapes",
            "supplier": "Fruity Co.",
            "stocks": 30,
            "price": 100,
            "onSale": true
        },
        {
            "name": "Apple",
            "supplier": "Apple Valley",
            "stocks": 0,
            "price": 20,
            "onSale": false
        },
        {
            "name": "Papaya",
            "supplier": "Fruity Co.",
            "stocks": 15,
            "price": 60,
            "onSale": true
        }
]);

// 1.) Use 3 stages aggregation pipeline to count the total number of fruits on sale, exclude the _id field.

// Code here:


// 2.) Use 3 stages aggregation pipeline to count the total number of fruits with stock more than 20, and exclude the _id field.

// Code here:


// 3.) Use the average operator to get the average price of fruits onSale per supplier.

// Code here:


// 4.) Use the max operator to get the highest price of a fruit per supplier.

// Code here:


// 5. Use the min operator to get the lowest price of a fruit per supplier.

// Code here:

db.fruits.aggregate([
        {
            $match: {"onSale": true}
        },
        {
            $group: {_id: "fruitOnSale", count: {$sum:1}}
        }
    ]);


db.fruits.aggregate([
        {
            $match: {"stocks": {$gte: 20}}
        },
        {
            $group: {_id: "total", count: {$sum:1}}
        }
    ]);

db.fruits.aggregate([
    {
        $match:{"onSale": true}
    },
    {
        $group: {_id: "$supplier", avgPrice: {$avg: "$price"}}
    }

    ])

db.fruits.aggregate([
    {
        $match: {"price": {$gte : 0}}
    },
    {
        $group: {_id: "$supplier", maxPrice: {$max: "$price"}}
    }
    ]);

db.fruits.aggregate([
    {
        $match: {"price": {$gte : 0}}
    },
    {
        $group: {_id: "$supplier", minPrice: {$min: "$price"}}
    }
    ]);