// PART 1

db.fruits.aggregate([
        {
            $match: {"onSale": true}
        },
        {
            $group: {_id: "fruitOnSale", count: {$sum:1}}
        },
        {
            $project: {"_id": 0}
        }
    ]);

// PART 2

db.fruits.aggregate([
        {
            $match: {"stocks": {$gte: 20}}
        },
        {
            $group: {_id: "total", count: {$sum:1}}
        },
        {
            $project: {"_id": 0}
        }
    ]);

// PART 3

db.fruits.aggregate([
    {
        $match:{"onSale": true}
    },
    {
        $group: {_id: "$supplier", avgPrice: {$avg: "$price"}}
    }

    ])

// PART 4

db.fruits.aggregate([
    {
        $group: {_id: "$supplier", maxPrice: {$max: "$price"}}
    }
    ]);

// PART 5

db.fruits.aggregate([
    {
        $group: {_id: "$supplier", minPrice: {$min: "$price"}}
    }
    ]);  